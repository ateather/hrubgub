This project is broken up into two sections that are independent of eachother.

Please install the latest version of node or you might get some errors.

# Instalation

```
cd client
npm install
cd ../server
npm install
```

# Running the client

Vue applications are a bit different from traditional html in that it "compiles" before it get run.
This is done through a module bundler called webpack.
There are watcher scripts that will detect all changes to managed files. 
So after starting the client, you do not have to restart the client commands.

TL;DR, run this once:
```
cd client
npm start
```

# Running the server
Simple node express server. You must restart this on every change. 
Remember to start the mongo instance first

```
cd server
sudo bash ./rundb
node src/app.js
```

# Connections

Client => `localhost:8080`

Server => `localhost:8081`

Make sure these ports are not in use

# lil notes

Open current directory in vscode: `code .`

TODO lists are in the src/component folders.

Just fyi, you'll see a lot of warnings, even though it says erros in the vue compile window. 
These are just trying to enforce the lint styling rules.
Although I'd recommend trying to follow them, you can ignore these rules.
You can turn off the warnings for a file by putting `/* es-lint disable */` before your code.