
import Api from '@/services/Api'

export default {
  fetchPosts () {
    return Api().get('posts')
  },
  fetchStoreItems (id) {
    return Api().get('stores/' + id)
  }
}
