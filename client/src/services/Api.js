/* es-lint disable */
import axios from 'axios'

const BASEURL = 'http://localhost:8081'
let create = () => {
  return axios.create({
    baseURL: BASEURL
  })
}

export default {
  tryLogin (vue, user, pass, cb) {
    vue.$http.get(BASEURL+'/login?username='+user+"&password="+pass).then((response) => {
        cb(response.data);
    });  
  },

  fetchStores () {
    return create().get('stores')
  },

  fetchStore (id) {
    return create().get('store/' + id)
  },

  fetchCart () {
    return create().get('cart')
  },

  checkout () {
    return create().get('checkout')
  },

  cartItem (vue, item, cost, count, cb) {
    JSON.stringify(console.log(item));

    vue.$http.get(BASEURL+'/cartItem?item='+item+'&count='+count+'&cost='+cost)
      .then(response => {
        cb(response.data);
      });
  },

  addStore (vue, store, cb) { 
    vue.$http.post(BASEURL+'/addStore', {
      store: store
    })
      .then(response => {
        cb(response.status == 200)
      })
  }

}
