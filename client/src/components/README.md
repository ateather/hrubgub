# TODO

## Data Structures
### Store
```javascript
{
    id: 0,
    name: "demo",
    address: "8150 baltimore ave, etc.",
    description: "",
    image: "http://via.placeholder.com/350x150",
    items: []
}
```

### Item
```javascript
{
    id: 0,
    name: "item",
    cost: 0.5,
    description: ""
}
```

### CartItem
```javascript
{
    item: {}, // Please dont include the image
    count: 1
}
```

## Stores.vue

A component that displays all of the stores available.

1. Get all stores displayed

Api Calls: `fetchStores()` returns: `[]` containing all stores

### 2. Style and Organize Display Cards
3. Link to Store Page

`href='/store/:id'`

## Store.vue

A component that will display all of the items available. 

Current store's id: `this.$route.params.id`

1. Display all store items

Api Calls: `fetchStore(store_id)` returns: Store with a matching id

### 2. Add selected item to cart
Api Calls: `cartItem (vue, item, count, cb)` where vue is just `this` and `cb` is the callback function

## Checkout.vue
Api Calls: `fetchCart()` returns: `[]` containing all CartItems

## AddStore.vue
Api Calls: `addStore(store)`

## Login.vue

1. Make the login text on the bar a component that checks the session.
2. Allow for registration

