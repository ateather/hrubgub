// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  html: {
    root: `http://localhost:8081`
  }
})

// Note avoid use of use of these, use vue alternatives
// require('./assets/jquery.min.js')
require('./assets/bootstrap.min.css')
// require('./assets/bootstrap.min.js')
