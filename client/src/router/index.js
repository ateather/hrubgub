import Vue from 'vue'
import Router from 'vue-router'
import VueResource from 'vue-resource'
import Store from '@/components/Store'
import Login from '@/components/Login'
import Stores from '@/components/Stores'
import Checkout from '@/components/Checkout'
import AddStore from '@/components/addStore'

Vue.use(Router)
Vue.use(VueResource)

export default new Router({
  routes: [
    {
      path: '/store/:id',
      name: 'Store',
      component: Store
    },
    {
      path: '/stores',
      name: 'Stores',
      component: Stores
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/checkout',
      name: 'Checkout',
      component: Checkout
    },
    {
      path: '/addStore',
      name: 'AddStore',
      component: AddStore
    }

  ]
})
