const express = require('express')
const cookieSession = require('cookie-session')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const shajs = require('sha.js')
const sha = shajs('sha256')
const mongo = require('mongodb').MongoClient
const mongoose = require('mongoose');
const DBURL = "mongodb://localhost:23456/hrubgub"
// npm install --save nodemon // to keep the server running 

mongoose.connect(DBURL);
mongoose.Promise = global.Promise;
let Schema = mongoose.Schema;

// Store Table
var StoreSchema = new Schema({
  id: String,
  name: String,
  address: String,
  description: String,
  image: String
});
let Stores = mongoose.model('Stores', StoreSchema);

// Item Table
var ItemSchema = new Schema({
  id: String,
  name: String,
  cost: Number,
  description: String,
  store: String
});
let Items = mongoose.model('Items', ItemSchema);


const app = express()
app.use(cookieSession({
  name: 'sesh',
  keys: ['sooosecure69'],
  maxAge: 24 * 60 * 60 * 1000
}));
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

let stores = {}
let session = {}

/* Class Definitions */
function Store(id, name) {
  this.id = id;
  this.name = name;
  this.items = [];
  this.address = "";
  this.description = "";
  this.image = "http://via.placeholder.com/350x150";

  this.addItem = item => {
    this.items.push(item);
  }

  this.setAddress = addr => {
    this.address = addr;
  }

}

function Item(name, cost) {
  this.name = name;
  this.cost = cost;
  this.id = -1;
  this.description = "";
  this.image = "";
}

function CartItem(item, count, cost) {
  this.item = item;
  this.count = count || 1;
  this.cost = cost;
}

function User(name, pass, salt) {
  this.user = name;
  this.pass = pass;
  this.salt = salt || "xxx";
  this.hash = sha.update(this.user + this.pass + this.salt).digest('hex') 
}

const DEMOUSER = new User("t", "terps");

let DemoStore = new Store("base", "Demo Mart");
DemoStore.description = "An example of base store";
DemoStore.addItem(new Item("Pizza", 4.20));
DemoStore.addItem(new Item("Bigger Pizza", 4.20));
DemoStore.setAddress("Memeville");
let DemoStore2 = new Store("ds2", "Demo Mart2");
DemoStore2.addItem(new Item("Not Pizza", 4.20));
DemoStore2.setAddress("Not Memeville");

let initStores = () => {
  Stores.find({}, (err, docs) => {
    if (err)
      throw err
    //console.log(JSON.stringify(docs, null, 4))
    docs.forEach(entry => {
      let aentry = entry
      //console.log("a: " + JSON.stringify(aentry))
      let newstore = new Store(aentry.id, aentry.name)
      newstore.description = aentry.description
      newstore.address = aentry.address
      newstore.image = aentry.image

      stores[newstore.id] = newstore

      Items.find({store: newstore.id}, (err2, iss) => {
        if (err2)
          throw err2

          if (iss.length > 0) {
            //console.log(JSON.stringify(iss), null, 4)
            iss.forEach(i => {
              let newitem = new Item(i.name, i.cost)
              newitem.id = i.id
              newitem.description = i.description
              //console.log(JSON.stringify(newitem, null, 4))
              stores[aentry.id].items.push(newitem)
            })
          }
          
      })
    })
  })

  //stores[DemoStore.id] = DemoStore;
  //stores[DemoStore2.id] = DemoStore2;
}

let isValidUser = user => {
  // query db
  return (user.user == DEMOUSER.user
      && user.pass == DEMOUSER.pass);
}

let addToCart = (item, req) => {
  let addme = new CartItem(item, parseInt(req.query.count));
  let cart = req.session.cart || [];
  cart.push(addme);
  req.session.cart = cart;
}

/* Server Bindings */
app.options('/login', (req, res, next) => {
  var headers = {};
  // headers["Access-Control-Allow-Origin"] = req.headers.origin;
  headers["Access-Control-Allow-Origin"] = "*";
  headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
  headers["Access-Control-Allow-Credentials"] = false;
  headers["Access-Control-Max-Age"] = '86400';
  headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept";
  res.writeHead(200, headers);
  res.end();
  next();
});

// Im not sure this is needed anymore
app.get('/login', (req, res) => {
  console.log(req.query.username + " - " + req.query.password);
  console.log(JSON.stringify(req.query, null, 4));
  let uploadedUser = new User(req.query.username, req.query.password);
  if (isValidUser(uploadedUser)) {
    let salt = "xyx"
    let hash = sha.update(req.query.username + req.query.password + salt).digest('hex')
    req.session.hash = hash;
    res.send({
      success: true,
      hash: hash
    })
    res.sendStatus(200);
  } else {
    res.sendStatus(401);
  }
  res.end()
})

// Send all stores
app.get('/stores', (req, res) => {
  // Might want to slim this down 
  // ie dont send items
  res.send(stores);
  res.end();
})

// Send store info from id
app.get('/store/:id', (req, res) => {
  let store = stores[req.params.id]
  if (store) {
    console.log(JSON.stringify(store,null,4))
    res.send(store)
  } else {
    res.sendStatus(404)
  }
  res.end()
})

// Get contents of cart
app.get('/cart', (req, res) => {
  console.log(JSON.stringify(session.cart));
  res.send(session.cart);
  res.end();
})

// Add item to cart
app.get('/cartItem', (req, res) => {
  let item = req.query.item;
  if (item) {
    let cost = parseFloat(req.query.cost);
    let addme = new CartItem(item, parseInt(req.query.count), cost);
    let cart = session.cart || [];
    cart.push(addme);
    session.cart = cart;
    res.sendStatus(200);
  }
  res.end();
})

app.post('/addStore', (req, res) => {
  console.log(JSON.stringify(req.body, null, 4))
  if (req.body.store) {
    let store = new Store(req.body.store.id, req.body.store.name)
    store.setAddress(req.body.store.address)
    store.description = req.body.store.description
    store.image = req.body.store.image
    //store.items = DemoStore.items //
    store.items = req.body.store.items

    stores[store.id] = store

    let add = new Stores({
      id: store.id,
      name: store.name,
      address: store.address,
      description: store.description,
      image: store.image
    });
    add.save((err, s) => {
      if (err)
        throw err
      //console.log(JSON.stringify(s))
    })

    store.items.forEach(i => {
      let iadd = new Items({
        store: store.id,
        id: i.id,
        name: i.name,
        cost: i.cost,
        description: i.description
      })
      iadd.save((err, s) => {
        if (err)
          throw err
        //console.log(JSON.stringify(s))
      })
    })

    

    res.sendStatus(200)
  } else {
    res.sendStatus(401)
  }
  res.end()
})

app.get('/checkout', (req,res)=> {
  session = {}
  res.sendStatus(200)
  res.end()
})

initStores();
app.listen(process.env.PORT || 8081)